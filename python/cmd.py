#!/usr/local/bin/python
import click
import os
import json
import signal
import paramiko
import time
import boto3
#####api function
def sshfunc(cmd,server):
    try:
      for i in server:
        print(i)
        s3_client = boto3.client('s3',region_name='ap-southeast-1')
        s3_client.download_file('zeta','lambda/test.pem', '/tmp/file.pem')
        k = paramiko.RSAKey.from_private_key_file("/tmp/file.pem")
        print("we are trying to get key")
        c = paramiko.SSHClient()
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("we are trying to connect")
        c.connect( hostname = i, username = "root", pkey = k )
        stdin , stdout, stderr = c.exec_command(cmd)
        print("connection done")
    except:
      print("issue in cmd")
def command():
    cmd = click.prompt(
        "Please enter your cmd",
        default=""
     )
    hosts= click.prompt(
        "Please enter your server ip in comma seperated format",
        default=""
    )
    click.confirm('Do you want to continue?', abort=True)
    click.echo('we will run cmd in server')
    server = hosts.split(",")
    status=sshfunc(cmd,server)
    print(status)

while 1:

  command()
  print("Press ctrl + c/z or wait 3 sec to enter data agaain")  # wait for SIGINT
  time.sleep(3)
