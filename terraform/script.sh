#!/bin/bash
# install nginx
sudo apt-get update
sudo apt-get install awscli -y
sudo apt-get install docker-ce -y
sudo systemctl start docker
sudo systemctl enable docker
