module "vpc" {
  source = "./modules/vpc/"
  vpc_name        = "zeta-vpc"
  vpc_cidr        = "172.12.0.0/16"

}
output "private_subnet_id"   { value = "${module.vpc.private_subnet_id}" }
output "public_subnet_id"   { value = "${module.vpc.public_subnet_id}" }
output "vpc_id"   { value = "${module.vpc.vpc_id}" }

module "sg" {
  source = "./modules/newsg/"
  vpc_id                  = module.vpc.vpc_id
  groups = [
    {
    name                    = "zeta-server"
    description             =  "zeta-server"
    environment             = "test"
    port = [{
     description = "zeta-server",
     port = 8090,
     cidr_blocks = ["172.12.0.0/16"],
    }]
  tags = {
    Provisioner = "Terraform"
   }
   }
  ]
}
output  "sg_id" { value = "${module.sg.*.sg_id}" }
module "awsrole" {
  source = "./modules/iam/"
  name   = "zeta-role"
}
output  "iam_arn" { value = "${module.awsrole.iam_arn}" }

data "template_file" "userdata" {
  template = file("./script.sh")
}

module "ec2" {
  source                      = "./modules/ec2"
  instance_count              = 1
  ami_id                      = "ami-010aff33ed5991201"
  instance_type               = "t3.small"
  vpc_security_group_ids      = module.sg.*.sg_id[0]
  subnet_id                   = module.vpc.private_subnet_id
  iam_instance_profile        = module.awsrole.iam_arn
  key_name                    = "zeta-server"
  associate_public_ip_address = true
  name                        = "zeta-server"
  user_data                   = data.template_file.userdata.rendered
  root_block_device = [{
    volume_type           = "gp3"
    volume_size           = 25
    delete_on_termination = true
    }
  ]
  tags = {
    Provisioner = "Terraform"
  }
  volume_tags = {
    Provisioner = "Terraform"
  }
}

