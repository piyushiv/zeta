resource "aws_iam_role" "zeta_role" {
  name = "${var.name}"
    assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": [ "ec2.amazonaws.com",
                    "ecs-tasks.amazonaws.com" ]
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
EOF
tags = merge(
    {
      "Name" = "${var.name}"
    }
  )
}
resource "aws_iam_policy" "policy" {
  name        = "${var.name}-policy"
  description = var.name
   policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject",
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::zeta-s3/*"
    }
  ]

}
EOT
}
resource "aws_iam_role_policy_attachment" "role-policy-attachment" {
  role       = aws_iam_role.zeta_role.name
  policy_arn = aws_iam_policy.policy.arn
}
output  "iam_arn" { value = "${aws_iam_role.zeta_role.arn}" }
