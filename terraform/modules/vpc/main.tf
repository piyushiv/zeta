resource "aws_vpc" "zeta-vpc" {

  assign_generated_ipv6_cidr_block = false
  enable_classiclink               = false
  enable_classiclink_dns_support   = false
  cidr_block                       = "${var.vpc_cidr}"
  enable_dns_hostnames             = true
  enable_dns_support               = true

  tags = {
    Name        = "${var.vpc_name}"
  }

}
output  "vpc_id" { value = "${aws_vpc.zeta-vpc.id}" }


data "aws_availability_zones" "az" {}

resource "aws_subnet" "public-web-a" {
  vpc_id                  = "${aws_vpc.zeta-vpc.id}"
  cidr_block              = "${cidrsubnet(var.vpc_cidr, 3, 2)}"
  availability_zone       = "${data.aws_availability_zones.az.names[0]}"
  map_public_ip_on_launch = true

  tags = {
    Name        = "${var.vpc_name}-public-web-a"
  }
}

output "public_subnet_id"   { value = "${aws_subnet.public-web-a.id}" }

resource "aws_subnet" "private-server-a" {
  vpc_id                  = "${aws_vpc.zeta-vpc.id}"
  cidr_block              = "${cidrsubnet(var.vpc_cidr, 4, 3)}"
  availability_zone       = "${data.aws_availability_zones.az.names[0]}"
  map_public_ip_on_launch = false

  tags = {
    Name        = "${var.vpc_name}-private-server-a"
  }
}
output "private_subnet_id"   { value = "${aws_subnet.private-server-a.id}" }



resource "aws_internet_gateway" "vpc-gw" {
  vpc_id = "${aws_vpc.zeta-vpc.id}"

  tags = {
    Name        = "${var.vpc_name}-igw"
  }
}
resource "aws_eip" "nat-eip-server-a" {
  vpc = true
}
##NAT gateway server a
resource "aws_nat_gateway" "natgateway-zeta-vpc" {
  allocation_id = "${aws_eip.nat-eip-server-a.id}"
  subnet_id     = "${aws_subnet.public-web-a.id}"
  tags = {
    Name        = "${var.vpc_name}-natgateway"
  }
}
resource "aws_route_table" "zeta-public-web" {
  vpc_id = "${aws_vpc.zeta-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.vpc-gw.id}"
  }
  tags = {
    Name        = "${var.vpc_name}-rt-public-web"
  }
}
resource "aws_route_table" "zeta-rt-private" {
  vpc_id = "${aws_vpc.zeta-vpc.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.natgateway-zeta-vpc.id}"
  }

  tags = {
    Name        = "${var.vpc_name}-rt-private"
    Provisioner = "Terrafrom"
  }
}
##public Subnet association with public routing table
resource "aws_route_table_association" "subnet-rt-web-a" {
  subnet_id      = "${aws_subnet.public-web-a.id}"
  route_table_id = "${aws_route_table.zeta-public-web.id}"
}

##Private subnet association with private routing tables
resource "aws_route_table_association" "subnet-rt-server-a" {
  subnet_id      = "${aws_subnet.private-server-a.id}"
  route_table_id = "${aws_route_table.zeta-rt-private.id}"
}
resource "aws_default_network_acl" "mini-prod" {
  default_network_acl_id = aws_vpc.zeta-vpc.default_network_acl_id


  ingress {
    protocol   = -1
    rule_no    = 150
    action     = "allow"
    cidr_block = aws_vpc.zeta-vpc.cidr_block
    from_port  = 0
    to_port    = 0
  }
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  }
  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  }
  ingress {
    protocol   = -1
    rule_no    = 250
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  subnet_ids = [aws_subnet.public-web-a.id, aws_subnet.private-server-a.id]
  tags = {
    Name        = "${var.vpc_name}-nacl"
  }
}
