variable "vpc_name" {
  description = "vpc_name"
  type        = string
  default     = null
}

variable "vpc_cidr" {
  default     = ""
}
