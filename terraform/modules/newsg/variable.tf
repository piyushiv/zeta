variable "name" {
  default = ""
}
variable "environment" {
  default = ""
}

variable "groups" {
  type        = any
  default = ""
}
variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
variable "port" {
  default = []
  type    = any
}
variable "vpc_id" {
  default     = ""
  type        = string
}
variable "subnet_id" {
  default = ""
  type        = any
}
