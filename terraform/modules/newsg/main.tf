resource "aws_security_group" "sg" {
  count = length(var.groups)
  name  = lookup(var.groups[count.index], "name", null)
  description = lookup(var.groups[count.index], "description", null)
  vpc_id      = var.vpc_id

  dynamic "ingress" {

    for_each = lookup(var.groups[count.index], "port", {})
    content {
      description = ingress.value.description
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = "tcp"
      cidr_blocks = ingress.value.cidr_blocks
    }
  }
  egress {
   protocol         = "-1"
   from_port        = 0
   to_port          = 0
   cidr_blocks      = ["0.0.0.0/0"]
   ipv6_cidr_blocks = ["::/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
  tags = merge(
    {
      "Name" = lookup(var.groups[count.index], "name", null)
    },
    var.tags,
  )
}
output  "sgid" {
             value = {
                  for sgid in aws_security_group.sg:
                  sgid.name => sgid.id
      }
}

output  "sg_id" { value = "${aws_security_group.sg.*.id}" }
